﻿using Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Linq.Customers.Core.Domain.Repositories.Abstractions;
using Otus.Teaching.Linq.Customers.Infrastructure.DataAccess;
using Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.DbInitialization;

namespace Otus.Teaching.Linq.Customers.Host.WebApi.Composition
{
    public static class DalExtensions
    {
        public static IServiceCollection AddDalDependencies(this IServiceCollection services)
        {
            services.AddScoped<IDbInitializer, DropAndCreateDbInitializer>();
            
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlite("Filename=OtusTeachingLinqCustomersDb.sqlite");
            });
            
            services.AddScoped<IDbInitializer, DropAndCreateDbInitializer>();
            services
                .AddScoped<ICustomerRepository, EfCustomerRepository>();

            return services;
        }
    }
}
