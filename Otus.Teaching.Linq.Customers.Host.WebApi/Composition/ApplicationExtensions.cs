﻿
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Linq.Customers.Core.Application.Services;
using Otus.Teaching.Linq.Customers.Core.Application.Services.Abstractions;

namespace Otus.Teaching.Linq.Customers.Host.WebApi.Composition
{
    public static class ApplicationExtensions
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services)
        {
             services
                .AddScoped<ICustomerService, CustomerEntityService>();

             return services;
        }
    }
}
