﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Linq.Customers.Core.Domain.Entities;
using Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.Mapping;

namespace Otus.Teaching.Linq.Customers.Infrastructure.DataAccess
{
    public class DataContext
        : DbContext
    {
        /// <summary>
        /// Customers
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CustomerMappingConfiguration());
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}