﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.Linq.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.Mapping
{
    public class CustomerMappingConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            
        }
    }
}