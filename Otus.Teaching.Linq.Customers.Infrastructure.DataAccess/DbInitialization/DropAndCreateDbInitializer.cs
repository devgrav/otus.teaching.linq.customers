﻿﻿﻿using System;
using System.Collections.Generic;
  using Otus.Teaching.Linq.Customers.Infrastructure.DataAccess;
  using Otus.Teaching.Linq.Customers.Core.Domain.Entities;

  namespace Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.DbInitialization
{
    public class DropAndCreateDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dbContext;

        public DropAndCreateDbInitializer(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Initialize()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();

            var customers = new List<Customer>()
            {
                new Customer()
                {
                    Id = Guid.Parse("5e331afd-0a4c-456d-82df-2ab48dc46f98"),
                    Channel = AcquisitionChannel.Email,
                    CreatedDate = DateTime.Now.AddHours(-3),
                    FullName = "Петров Иван",
                    Email = "petrov@yandex.ru",
                    IsActive = true
                },
                new Customer()
                {
                    Id = Guid.Parse("18484ae0-24c1-47b5-90a6-4b5866507e13"),
                    Channel = AcquisitionChannel.Sms,
                    CreatedDate = DateTime.Now.AddDays(-10),
                    FullName = "Иванов Игорь",
                    Email = "ivanov@mail.ru",
                    IsActive = false
                },
                new Customer()
                {
                    Id = Guid.Parse("c3663719-bf10-4716-8ea3-a3f3de4c8c8d"),
                    Email = "sergeev@info.ru",
                    Channel = AcquisitionChannel.Street,
                    CreatedDate = DateTime.Now.AddDays(-5),
                    FullName = "Сергеев Иван",
                    IsActive = false
                }
            };

            _dbContext.Customers.AddRange(customers);

            _dbContext.SaveChanges();
        }
    }
}
