﻿﻿﻿namespace Otus.Teaching.Linq.Customers.Infrastructure.DataAccess.DbInitialization
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
