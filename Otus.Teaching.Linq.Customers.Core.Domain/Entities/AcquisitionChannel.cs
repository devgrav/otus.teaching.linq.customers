﻿﻿﻿namespace Otus.Teaching.Linq.Customers.Core.Domain.Entities
{
    public enum AcquisitionChannel
    {
        Street = 1,
        
        Email =2,
        
        Sms = 3
    }

}
