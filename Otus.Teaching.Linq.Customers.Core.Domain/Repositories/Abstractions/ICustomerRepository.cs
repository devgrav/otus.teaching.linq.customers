﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Linq.Customers.Core.Domain.Dto;
using Otus.Teaching.Linq.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Linq.Customers.Core.Domain.Repositories.Abstractions
{
    public interface ICustomerRepository
        : IRepository<Customer>
    {
        Task<CustomersForListDto> GetAllByFilterAsync(CustomerFilterDto filterDto);
    }
}