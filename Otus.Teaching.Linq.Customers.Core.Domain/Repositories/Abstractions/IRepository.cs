﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Linq.Customers.Core.Domain.Repositories.Abstractions
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);
    }
}