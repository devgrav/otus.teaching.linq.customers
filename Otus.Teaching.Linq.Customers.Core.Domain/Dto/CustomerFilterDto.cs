﻿using System;
using Otus.Teaching.Linq.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Linq.Customers.Core.Domain.Dto
{
    public class CustomerFilterDto
    {
        public string FullName { get; set; }

        public AcquisitionChannel? Channel { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? CreatedDateLessThenOrEqual { get; set; }
        
        public DateTime? CreatedDateMoreThenOrEqual { get; set; }

        public bool SortByFullName  { get; set; }

        public bool IsActive { get; set; }
    }
}