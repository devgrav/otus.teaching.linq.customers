﻿using System.Collections.Generic;

namespace Otus.Teaching.Linq.Customers.Core.Domain.Dto
{
    public class CustomersForListDto
    {
        public List<CustomersForListItemDto> Items { get; set; }
    }
}
