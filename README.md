# Otus.Teaching.Linq.Customers

Демо проект для занятия про LINQ на примере написания фильтра по списку клиентов. Позволяет получить список клиентов, отфильтрованный список и клиента по Id.

## Otus.Teaching.Linq.Customers.Core.Application
Проект с основной логикой приложения

## Otus.Teaching.Linq.Customers.Core.Domain
Проект с основными сущностями и интерфейсами

## Otus.Teaching.Linq.Customers.Host.WebApi
Проект с API

## Otus.Teaching.Linq.Customers.Infrastructure.DataAccess
Доступ к данным
